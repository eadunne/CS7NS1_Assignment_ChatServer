echo "The run script has started." 

if [ -z "$1" ]
 then 
  echo "Error: you have not specified a port number for the server to use."
 else
  echo "The server will now start up using IP Address 134.226.44.50."
  echo "Your chosen port number is: " "$1"
  ./Main 134.226.44.50 "$1"
fi

