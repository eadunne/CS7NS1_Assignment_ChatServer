Name: Éamon Dunne
Student Number: 13324777

This repository contains a Linux executable file, entitled "Main". The executable has already been compiled. It should be capable of running on a Linux system.

Run the server by entering "./run.sh" followed by your choice of port number into a Linux command prompt.