module Main where  
   
import Network.Socket  
import System.IO  
import Control.Concurrent  
import Control.Monad (when)  
import Control.Monad.Fix (fix)  
import Control.Exception  
  
import System.Exit 
 
import Text.Read (readMaybe)  
  
import Data.List.Split  
import Data.List  
  
import Control.Monad  
  
import System.Environment  
  
type Termination = String  
  
newtype TerminationState = TerminationState (MVar Termination)  

fst_3 :: (a, b, c) -> a
fst_3 (a, _, _) = a
  
snd_3 :: (a, b, c) -> b  
snd_3 (_, b, _) = b  
  
thd_3 :: (a, b, c) -> c  
thd_3 (_, _, c) = c     
 
fromString :: String -> Maybe PortNumber 
fromString s = fromInteger <$> readMaybe s 
  
-- Main: starts up the server  
main :: IO ()  
main = do  
    hSetBuffering stdout LineBuffering  -- Need this line so that the program does not wait until termination before it prints to console  
      
    print "Starting up"  
      
    -- Get command line arguments  
    args <- getArgs  
    let arg_ipAddress_string = args !! 0  
    let arg_port_string = args !! 1  
      
    let arg_ipAddress_int = read arg_ipAddress_string :: Int       -- Convert IP address from string to Int    
    let arg_port_maybe = fromString arg_port_string     -- Convert port number from string to PortNumber 
    let arg_port = case arg_port_maybe of Just a -> a 
                                          Nothing -> 0     
  
    sock <- socket AF_INET Stream 0    -- Create socket  
    setSocketOption sock ReuseAddr 1   -- Make socket immediately reusable - eases debugging  
    bind sock (SockAddrInet arg_port iNADDR_ANY)   -- Listen on the specified TCP port  
    listen sock 2   -- Set a max of 2 queued connections  
    controlChannel <- newChan -- 'controlChannel' variable: a newly created channel  
     
    _ <- forkIO $ fix $ \loop -> do  
      (_, _) <- readChan controlChannel  
      
      loop  
        
    m <- newEmptyMVar   -- terminationMVar  
    n <- newEmptyMVar   -- newClientMVar  
        
    terminationLoop sock controlChannel m n  -- Begin the termination loop, which determines when the server itsel shuts down
      
type Msg = (Int, String)  
  

-- TerminationLoop: keeps the server running until the KILL_SERVICE command is received 
terminationLoop :: Socket -> Chan Msg -> MVar (Int) -> MVar (Chan Msg) -> IO ()  
terminationLoop sock controlChannel terminationMVar newClientMVar = do  
  
    forkIO (controlLoop sock controlChannel newClientMVar [controlChannel] [] [] 0)  -- Fork off controlLoop for messaging clients  
    forkIO (mainLoop sock controlChannel terminationMVar newClientMVar 0)   -- Fork off mainLoop for listening for new client connections  
      
    fix $ \loop -> do  
        checkMVar <- tryTakeMVar terminationMVar    -- Keep checking to see if the server should terminate  
        when (checkMVar == Nothing) loop  
          
  
-- mainLoop: Listens for connections.  
-- Passes client connections to runConn for handling  
mainLoop :: Socket -> Chan Msg -> MVar(Int) -> MVar(Chan Msg) -> Int -> IO ()  -- Mainloop takes a Socket, a (Chan, Msg), two MVars and an int, and returns IO ()  
mainLoop sock controlChannel terminationMVar newClientMVar numberOfClientJoins = do   
  
    let writeToControlChannel msg = writeChan controlChannel (0, msg)  
  
    print "Listening for new connections..."  
    conn <- accept sock     -- Accept a connection and handle it  
    print "A new client has connected to the server"  
      
    -- Create a private channel for use by this client. The server will write to this channel when it needs to say something to the client.  
    privateClientChannel <- newChan  
    privateClientChannelCopy <- dupChan privateClientChannel    -- Duplicate the private client channel  
    forkIO (putMVar newClientMVar privateClientChannelCopy)  -- Put the copy of the private client channel in an MVar so that controlLoop can access it. putMVar blocks until it is successful, so put it in a new thread.  
    writeToControlChannel("NEW_CLIENT")  -- Notify the controlLoop that a new client has joined
      
    let newNumberOfJoins = numberOfClientJoins + 1  
      
    forkIO (runConn conn controlChannel privateClientChannel newNumberOfJoins terminationMVar)   -- Pass the control channel and the private client channel to runConn in a new thread  
      
    mainLoop sock controlChannel terminationMVar newClientMVar newNumberOfJoins    -- Repeat the mainLoop -> mainLoop(sock, controlChannel, (msgNum + 1))  
   
--runConn: Handles client connections.  
-- Client input that needs to be acted upon is written to the shared controlChannel.  
-- The controlChannel is read by the controlLoop, which acts upon client input as necessary.  
runConn :: (Socket, SockAddr) -> Chan Msg -> Chan Msg -> Int -> MVar(Int) -> IO ()  
runConn (sock, _) controlChannel privateClientChannel clientNumber terminationMVar = do  

    -- Configure writing to the control channel  
    let writeToControlChannel msg = writeChan controlChannel (clientNumber, msg)  
    hdl <- socketToHandle sock ReadWriteMode 
    let bufferSize = 1024
    let bufferSizeMaybe = Just bufferSize 
    hSetBuffering hdl (BlockBuffering bufferSizeMaybe)
    
    safeToDisconnectMVar <- newEmptyMVar
      
    commLine <- dupChan privateClientChannel   -- dupChan: Duplicate the 'controlChannel' channel - data written to either channel will be available to both - creates a broadcast  
      
    -- Fork off a thread for reading from the 'commLine' channel - this thread will listen for writes to the 'commLine' channel and write them to 'hdl'  
    reader <- forkIO $ fix $ \loop -> do 
    
        (nextNum, line) <- readChan commLine 

        case (line) of
             
             -- Special message: If this message is written to the client's private channel by the controlLoop, runConn will disconnect the client
             "ControlLoop:Disconnect" -> do
                                            putMVar safeToDisconnectMVar 999    
                                            print "Reader loop - safe to terminate connection" 
                                            
             _ -> hPutStr hdl line -- This writes the input from the 'commLine' chanel to 'hdl'
 
        hFlush hdl >> loop
          
    -- Read input from 'hdl' and take action (including printing stuff for the client)  
    handle (\(SomeException _) -> return ()) $ fix $ \loop -> do 

        line <- hGetLine hdl    
          
        -- Get command line arguments  
        args <- getArgs  
        let arg_ipAddress_string = args !! 0  
        let arg_port_string = args !! 1 
 
        -- Special case: HELO text 
        let splitSpace = splitOn " " line 
        if (splitSpace !! 0 == "HELO") 
        then do 
                 let helo_string = line ++ "\nIP:" ++ arg_ipAddress_string ++ "\nPort:" ++ arg_port_string ++ "\nStudentID:13324777"  
                 print helo_string 
                 hPutStr hdl helo_string
                 hFlush hdl >> loop
        else return () 
         

        let splitString = splitOn "\\n" line    -- Split input on newline character 
                 
        case (splitString !! 0) of  -- Get first line of input to figure out what type of request has been made  
            -- If this message is sent, shut down the server  
             "KILL_SERVICE" -> putMVar terminationMVar 999    
            -- If this message is sent, disconnect the client  
             "DISCONNECT: 0" -> do


                                   print "Disconnect message received"
                                   line2 <- hGetLine hdl
                                   line3 <- hGetLine hdl
                                   let controlChannelInput = line ++ "\\n" ++ line3
                                   writeToControlChannel (controlChannelInput) 
                                   print "Finished write to control channel. runConn is now waiting for safeToDisconnectMVar"
                                   
                                   
                                   -- Wait for confirmation from the control loop that all "client left" messages have been sent before disconnecting
                                   checkMVar <- takeMVar safeToDisconnectMVar -- Blocks until the MVar is written to by the reader loop
                                    --No loop here - the client will disconnect
                                   print "Client is disconnecting..."
               
             _ -> do 
                    -- Check for other message formats
                    let colonSplit = splitOn ":" line -- Split the line of input on colon character  
                    case (colonSplit !! 0) of
                    
                         "JOIN_CHATROOM" -> do
                             print "Join chatroom message received"
                             line2 <- hGetLine hdl
                             line3 <- hGetLine hdl
                             line4 <- hGetLine hdl
                             
                             let controlChannelInput = line ++ "\\n" ++ line2 ++ "\\n" ++ line3 ++ "\\n" ++  line4
                             writeToControlChannel (controlChannelInput) >> loop

                         "LEAVE_CHATROOM" -> do
                             print "Leave chatroom message received"

                             line2 <- hGetLine hdl
                             line3 <- hGetLine hdl
                             
                             let controlChannelInput = line ++ "\\n" ++ line2 ++ "\\n" ++ line3
                             writeToControlChannel (controlChannelInput) >> loop

                         "CHAT" -> do
                             print "Chat message received"
                             line2 <- hGetLine hdl
                             line3 <- hGetLine hdl
                             line4 <- hGetLine hdl
                             
                             let controlChannelInput = line ++ "\\n" ++ line2 ++ "\\n" ++ line3 ++ "\\n" ++ line4
                             writeToControlChannel (controlChannelInput) >> loop
                             
                         _ -> print "Client input did not match any formats" >> loop
                         
             
    print "Client is continuing disconnection process..."          
    killThread reader       -- Kill after the loop ends
    hClose hdl                 -- Close the handle  
      
  
-- controlLoop: Reads from the controlChannel shared by all clients.  
-- Acts upon input to the controlChannel.  
controlLoop :: Socket -> Chan Msg -> MVar(Chan Msg) -> [Chan Msg] -> [(Int, [Int], [String])] -> [String] -> Int -> IO ()  -- Controlloop takes a Socket, a (Chan, Msg), an int(testNum) and another int, and returns IO ()  
controlLoop sock controlChannel newClientMVar clientChannels chatrooms chatroomNames numberOfClients = do   
      
    let privateMessage msg privateChannel = writeChan privateChannel (0, msg)  
      
    commLine_ControlLoop <- dupChan controlChannel  
    (clientNumber, line) <- readChan commLine_ControlLoop -- Read the next line of input from the controlChannel  
      
    checkMVar <- tryTakeMVar newClientMVar    -- Check to see if a new client has been added  
      
    -- Update list of clients if a new client has been added
    let updatedListOfClientChannels = case checkMVar of Just a -> clientChannels ++ [a]  
                                                        Nothing -> clientChannels  
                 
    let oldNumberOfClients = numberOfClients  
    let updatedNumberOfClients = case checkMVar of Just a -> numberOfClients + 1  
                                                   Nothing -> numberOfClients  
      
    let lines_list = splitOn "\\n" line    -- Split input on newline character to get individual lines  
          
    let firstLine_list = splitOn ":" (lines_list !! 0) -- Split the first line of input on colon character  
           
    case (firstLine_list !! 0) of  
     "NEW_CLIENT" -> do  
                        controlLoop sock controlChannel newClientMVar updatedListOfClientChannels chatrooms chatroomNames updatedNumberOfClients -- Special message from main loop: update client channel list  
     -- If this message is sent, disconnect the client after sending a message to every chatroom they are in
     "DISCONNECT" -> do
                           let secondLine_list = splitOn ":" (lines_list !! 1) -- Split the second line of input on colon chracter
                           let client_name = secondLine_list !! 1


                           forM_ chatrooms $ \chatroom -> do  
                               let chatroom_clientIDs = snd_3 (chatroom)  
                               let chatroom_clientNames = thd_3 (chatroom) 
                               
                               let existing_clientName_maybe = findIndex (==client_name) chatroom_clientNames  
                              
                               let existing_clientName_index = case (existing_clientName_maybe) of Just a -> a  
                                                                                                   Nothing -> -1 


                               if (existing_clientName_index > -1)
                               then do
                                        let evaluatedName = chatroom_clientNames !! existing_clientName_index
                                        
                                        -- Send a message to every client in the chatroom
                                        let chatroom_id = fst_3 (chatroom)
                                        let clientLeftMessage = "CHAT:" ++ show chatroom_id ++ "\nCLIENT_NAME:" ++ client_name ++ "\nMESSAGE:" ++ client_name ++ " has left this chatroom.\n\n" 
                                        
                                        forM_ chatroom_clientIDs $ \i -> do  
                                            let thisClientName_maybe = findIndex (==i) chatroom_clientIDs
                                            let thisClientName_index = case (thisClientName_maybe) of Just a -> a
                                                                                                      Nothing -> -1
                                            let thisClientName = chatroom_clientNames !! thisClientName_index
                                            privateMessage clientLeftMessage $! updatedListOfClientChannels !! i 
                                            
                                            
                               else return ()
                                      
                           -- Tell the client's reader loop to terminate the client's connection
                           let disconnectMessage = "ControlLoop:Disconnect"
                           privateMessage disconnectMessage $! updatedListOfClientChannels !! clientNumber
                           
                           -- Loop  
                           controlLoop sock controlChannel newClientMVar updatedListOfClientChannels chatrooms chatroomNames numberOfClients  
                                



     -- If this message is sent, let the client join the specified chatroom  
     "JOIN_CHATROOM" -> do  
                            let chatroom_name = firstLine_list !! 1   
                              
                            let secondLine_list = splitOn ":" (lines_list !! 3) -- Split the fourth line of input on colon character  
                            let client_name = secondLine_list !! 1  
                              
                            let existing_chatroom_maybe = findIndex (==chatroom_name) chatroomNames  
                              
                            let existing_chatroom_index = case (existing_chatroom_maybe) of Just a -> a  
                                                                                            Nothing -> -1  
                                                                                              
                            if (existing_chatroom_index > -1)  
                                then do  
                                    let client_ID = clientNumber
                                      
                                    -- Get the (old) list of client IDs connected to this chatroom  
                                    let chatroom = chatrooms !! existing_chatroom_index  
                                    let chatroom_oldClientIDs = snd_3 (chatroom)  
                                    let chatroom_oldClientNames = thd_3 (chatroom)  
                                      
                                    -- Update list of chatrooms: update this chatroom's list of client IDs  
                                    let chatroom_updatedClientIDs = chatroom_oldClientIDs ++ [client_ID]  
                                    let chatroom_updatedClientNames = chatroom_oldClientNames ++ [client_name]  
                                    let new_and_updated_chatroom_entry = (existing_chatroom_index + 1, chatroom_updatedClientIDs, chatroom_updatedClientNames)  
                                      
                                    -- Split the (old) list of chatrooms  
                                    let chatrooms_half_1 = take existing_chatroom_index chatrooms  
                                    let chatrooms_half_2 = drop existing_chatroom_index chatrooms  
                                      
                                    -- Remove the old chatroom entry  
                                    let half_2_withOldEntryGone = drop 1 chatrooms_half_2   
                                    -- Add the new chatroom entry  
                                    let half_2_updated = [new_and_updated_chatroom_entry] ++ half_2_withOldEntryGone  
                                    -- Combine to get the updated list of chatrooms  
                                    let updatedChatroomsList = chatrooms_half_1 ++ half_2_updated  
                                      
                                    -- List of chatroom names is unchanged  
                                    let updatedChatroomNamesList = chatroomNames  
                                    -- List of client channels has already been updated  
                                    -- Number of clients has already been updated  
                                      
                                    -- Get command line arguments  
                                    args <- getArgs  
                                    let arg_ipAddress_string = args !! 0  
                                    let arg_port_string = args !! 1  
                                      
                                    -- Send a message to the client to inform them that they have joined the chatroom  
                                    let new_chatroom_id = existing_chatroom_index + 1  
                                    let joinedMessage = "JOINED_CHATROOM:" ++ chatroom_name ++ "\nSERVER_IP:" ++ arg_ipAddress_string ++ "\nPORT:" ++ arg_port_string ++ "\nROOM_REF:" ++ show new_chatroom_id ++ "\nJOIN_ID:" ++ show client_ID ++ "\n"
                                    
                                    privateMessage joinedMessage $! updatedListOfClientChannels !! client_ID  
                                      
                                    -- Send a message to every client in the chatroom  
                                      
                                    forM_ chatroom_updatedClientIDs $ \i -> do  
                                    
                                        let clientJoinMessage = "CHAT:" ++ show new_chatroom_id ++ "\nCLIENT_NAME:" ++ client_name ++ "\nMESSAGE:" ++ client_name ++ " has joined this chatroom.\n\n"
                                        
                                        privateMessage clientJoinMessage $! updatedListOfClientChannels !! i  
                                      
                                    print "Finished sending messages to clients"  
                                      
                                    -- Loop  
                                    print "------------------END OF CONTROL LOOP--------------------------------"  
                                    controlLoop sock controlChannel newClientMVar updatedListOfClientChannels updatedChatroomsList updatedChatroomNamesList updatedNumberOfClients  
                              
                                      
                                else do   
                                    -- Determine the ID for the chatroom being created  
                                    let chatroom_ID = length chatrooms + 1    -- New chatroom - create a new ID  
                                    let client_ID = clientNumber  
                                      
                                    let chatroom = (chatroom_ID, [client_ID], [client_name]) -- New chatroom - populated only by the client that requested it  
                                      
                                    -- Update list of chatrooms  
                                    let updatedChatroomsList = chatrooms ++ [chatroom]  
                                    -- Update list of chatroom names  
                                    let updatedChatroomNamesList = chatroomNames ++ [chatroom_name]  
                                    -- List of client channels has already been updated  
                                    -- Number of clients has already been updated  
                                      
                                    -- Send a message to every client in the chatroom (currently, there is only the client that just joined)  
                                    let clientInChatroom_ID = snd_3 chatroom    -- snd_3: Get second element of triple  
                               
                                    let numberOfChannels = length updatedListOfClientChannels  
                                      
                                    -- Get command line arguments  
                                    args <- getArgs  
                                    let arg_ipAddress_string = args !! 0  
                                    let arg_port_string = args !! 1  
                                      
                                    -- Send a message to the client to inform them that they have joined the chatroom  
                                    let joinedMessage = "JOINED_CHATROOM:" ++ chatroom_name ++ "\nSERVER_IP:" ++ arg_ipAddress_string ++ "\nPORT:" ++ arg_port_string ++ "\nROOM_REF:" ++ show chatroom_ID ++ "\nJOIN_ID:" ++ show client_ID ++ "\n"  
                                    
                                    privateMessage joinedMessage $! updatedListOfClientChannels !! client_ID 

                                    -- Send another message to the client  
                                    let printMessage = "Sending another private message to the client in this chatroom: " ++ chatroom_name  
                                    print printMessage  

                                    let clientJoinMessage = "CHAT:" ++ show chatroom_ID ++ "\nCLIENT_NAME:" ++ client_name ++ "\nMESSAGE:" ++ client_name ++ " has joined this chatroom.\n\n"
                                    
                                    privateMessage clientJoinMessage $! updatedListOfClientChannels !! client_ID 
                              
                                    -- Loop  
                                    controlLoop sock controlChannel newClientMVar updatedListOfClientChannels updatedChatroomsList updatedChatroomNamesList updatedNumberOfClients  
                              
     "LEAVE_CHATROOM" -> do  
                              
                            let chatroom_ID_string = firstLine_list !! 1  
                            let chatroom_ID = read chatroom_ID_string :: Int    -- Convert from string to int  
                              
                            if (chatroom_ID > -1)  
                                then do  
                                    let existing_chatroom_index = chatroom_ID - 1  
                                    let secondLine_list = splitOn ":" (lines_list !! 2) -- Split the third line of input on colon character  
                                    let client_name = secondLine_list !! 1  
                                      
                                    let client_ID = clientNumber  
                                      
                                    -- Get the (old) list of client IDs connected to this chatroom  
                                    let chatroom = chatrooms !! existing_chatroom_index  
                                    let chatroom_oldClientIDs = snd_3 (chatroom)  
                                    let chatroom_oldClientNames = thd_3 (chatroom)  
                                      
                                    let existing_client_maybe = findIndex (==client_name) chatroom_oldClientNames  
                              
                                    let existing_client_index = case (existing_client_maybe) of Just a -> a  
                                                                                                Nothing -> -1  
                                      
                                    -- Update list of chatrooms: update this chatroom's list of client IDs  
                                    -- Split the (old) list of client IDs  
                                    let clientIDs_half_1 = take existing_client_index chatroom_oldClientIDs  
                                    let clientIDs_half_2 = drop existing_client_index chatroom_oldClientIDs  
                                      
                                    -- Remove the old client ID entry  
                                    let half_2_withOldEntryGone = drop 1 clientIDs_half_2  
                                    -- Combine to get the updated list of clientIDs  
                                    let chatroom_updatedClientIDs = clientIDs_half_1 ++ half_2_withOldEntryGone  
                                      
                                    -- Update list of chatrooms: update this chatroom's list of client names  
                                    -- Split the (old) list of client names  
                                    let clientNames_half_1 = take existing_client_index chatroom_oldClientNames  
                                    let clientNames_half_2 = drop existing_client_index chatroom_oldClientNames  
                                      
                                    -- Remove the old client name entry  
                                    let half_2_withOldEntryGone_names = drop 1 clientNames_half_2  
                                    -- Combine to get the updated list of client names  
                                    let chatroom_updatedClientNames = clientNames_half_1 ++ half_2_withOldEntryGone_names  
                                      
                                    let new_and_updated_chatroom_entry = (existing_chatroom_index + 1, chatroom_updatedClientIDs, chatroom_updatedClientNames)  
                                      
                                    -- Split the (old) list of chatrooms  
                                    let chatrooms_half_1 = take existing_chatroom_index chatrooms  
                                    let chatrooms_half_2 = drop existing_chatroom_index chatrooms  
                                      
                                    -- Remove the old chatroom entry  
                                    let half_2_withOldEntryGone = drop 1 chatrooms_half_2   
                                    -- Add the new chatroom entry  
                                    let half_2_updated = [new_and_updated_chatroom_entry] ++ half_2_withOldEntryGone  
                                    -- Combine to get the updated list of chatrooms  
                                    let updatedChatroomsList = chatrooms_half_1 ++ half_2_updated  
                                      
                                    -- Send a message to the client to inform them that they have left the chatroom  
                                    let new_chatroom_id = existing_chatroom_index + 1  
                                    let leftMessage = "LEFT_CHATROOM:" ++ show new_chatroom_id ++ "\nJOIN_ID:" ++ show client_ID ++ "\n"  
                                    privateMessage leftMessage $! updatedListOfClientChannels !! client_ID   
                                      
                                    -- Send a message to every other client in the chatroom
                                    let clientLeftMessage = "CHAT:" ++ show new_chatroom_id ++ "\nCLIENT_NAME:" ++ client_name ++ "\nMESSAGE:" ++ client_name ++ " has left this chatroom.\n\n" 
                                    forM_ chatroom_updatedClientIDs $ \i -> do  
                                        privateMessage clientLeftMessage $! updatedListOfClientChannels !! i  
                                 
                                    -- Send another message to the client that just left
                                    privateMessage clientLeftMessage $! updatedListOfClientChannels !! client_ID                                   
                                      
                                    -- Loop 
                                    controlLoop sock controlChannel newClientMVar updatedListOfClientChannels updatedChatroomsList chatroomNames updatedNumberOfClients  
                              
                                      
                                else do   
                                    print "That chatroom doesn't exist!"  
                              
                                    let updatedChatroomsList = chatrooms  
                                    let updatedChatroomNamesList = chatroomNames  
                                    -- Loop  
                                    print "------------------END OF CONTROL LOOP--------------------------------"  
                                    controlLoop sock controlChannel newClientMVar updatedListOfClientChannels updatedChatroomsList updatedChatroomNamesList updatedNumberOfClients  
                              
     "CHAT" -> do  
            let secondLine_list = splitOn ":" (lines_list !! 1) -- Split the second line of input on colon character 
            let thirdLine_list = splitOn ":" (lines_list !! 2) -- Split the third line of input on colon character 
              
              
            let message_split_1 = splitOn "MESSAGE:" (line) -- Split the input on "MESSAGE:"  
            let message_line = message_split_1 !! 1 -- Get everything after the "MESSAGE:" string  
            let message_split_2 = splitOn "\\n\\n" message_line  
              
            let client_message = message_split_2 !! 0  
              
            -- Determine the ID for the chatroom being joined to  
            let chatroom_ID_string = firstLine_list !! 1  
            let chatroom_ID = read chatroom_ID_string :: Int    -- Convert from string to int  
              
            let client_ID = clientNumber  
            
            -- Get the list of client IDs connected to this chatroom  
            let chatroom_index = chatroom_ID - 1  
            let chatroom = chatrooms !! chatroom_index  
            let chatroom_clientIDs = snd_3 (chatroom)  
            let chatroom_names = thd_3 (chatroom)  
            let client_index = client_ID - 1  
            let client_name = thirdLine_list !! 1 --chatroom_names !! client_index  
                                      
            -- Send a message to every client in the chatroom  
            forM_ chatroom_clientIDs $ \i -> do 

                let messageToSend = "CHAT:" ++ show chatroom_ID ++ "\nCLIENT_NAME:" ++ client_name ++ "\nMESSAGE:" ++ client_message ++ "\n\n"
                      
                privateMessage messageToSend $! updatedListOfClientChannels !! i  
                --let printMessage = "Sent message to client number" ++ show i  
                --print printMessage  
                                        
            controlLoop sock controlChannel newClientMVar updatedListOfClientChannels chatrooms chatroomNames updatedNumberOfClients  
              
              
     _ -> do   
            let printMessage = "controlLoop: input did not match any format. Received:" ++ (firstLine_list !! 0)  
            print printMessage  
      
            controlLoop sock controlChannel newClientMVar updatedListOfClientChannels chatrooms chatroomNames updatedNumberOfClients 
    
